import Loading from 'views/Loading'
import { Suspense, lazy } from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import { useAuth } from 'core'
import { Main as MainLayout, Minimal as MinimalLayout, Admin as AdminLayout } from 'layouts'
import { PrivateRoute, PublicRoute } from 'components'
import AdminRoute from 'components/AdminRoute'
// import AdminRoute from 'components/AdminRoute'

const HomeView = lazy(() => import('views/Home'))
const CartView = lazy(() => import('views/Cart'))
const LoginView = lazy(() => import('views/Login'))
const NotFoundView = lazy(() => import('views/NotFound'))
const RegisterView = lazy(() => import('views/Register'))
const ProductView = lazy(() => import('views/Product'))
const CategoryView = lazy(() => import('views/Category'))
const DetailsView = lazy(() => import('views/Cart/components/Details'))
const OrderDetails = lazy(() => import('views/OrderDetails'))
const AdminView = lazy(() => import('views/Admin'))

export const PageURLs = {
  Cart: '/cart',
  Login: '/login',
  NotFound: '/404',
  Register: '/register',
  Product: '/products/:product',
  Categry: '/products/:categoryId',
  Details: '/details',
  OrderDetails: '/orders/:orderId',
  Admin: '/admin',
  AdminOrders: '/admin/orders',
}

const RoutesComponent = () => {
  const { isAuthenticated, loadingAuth, user } = useAuth()

  return !loadingAuth ? (
    <Suspense
      fallback={
        isAuthenticated ? (
          isAuthenticated && user.userGroup === 0 ? (
            <AdminLayout isSuspense={true} />
          ) : (
            <MainLayout isSuspense={true} />
          )
        ) : (
          <MinimalLayout isSuspense={true}>
            <Loading />
          </MinimalLayout>
        )
      }
    >
      <Routes>
        <Route path="/admin" element={isAuthenticated ? <AdminLayout /> : <Navigate to={PageURLs.Login} />}>
          <Route
            index
            element={
              <AdminRoute>
                <AdminView />
              </AdminRoute>
            }
          />
          <Route
            path={PageURLs.AdminOrders}
            element={
              <PrivateRoute>
                <DetailsView />
              </PrivateRoute>
            }
          />
        </Route>
        <Route path="/" element={<MainLayout />}>
          <Route
            index
            element={
              <PrivateRoute>
                <HomeView />
              </PrivateRoute>
            }
          />
          <Route
            path={PageURLs.Cart}
            element={
              <PrivateRoute>
                <CartView />
              </PrivateRoute>
            }
          />
          <Route
            path={PageURLs.CheckOut}
            element={
              <PrivateRoute>
                <CartView />
              </PrivateRoute>
            }
          />
          <Route
            path={PageURLs.Product}
            element={
              <PrivateRoute>
                <ProductView />
              </PrivateRoute>
            }
          />
          <Route
            path={PageURLs.Categry}
            element={
              <PrivateRoute>
                <CategoryView />
              </PrivateRoute>
            }
          />
          <Route
            path={PageURLs.Details}
            element={
              <PrivateRoute>
                <DetailsView />
              </PrivateRoute>
            }
          />
          <Route
            path={PageURLs.OrderDetails}
            element={
              <PrivateRoute>
                <OrderDetails />
              </PrivateRoute>
            }
          />

          <Route path="*" element={<NotFoundView />} />
        </Route>
        <Route path="/" element={<MinimalLayout />}>
          <Route
            path={PageURLs.Login}
            element={
              <PublicRoute>
                <LoginView />
              </PublicRoute>
            }
          />
          <Route
            path={PageURLs.Register}
            element={
              <PublicRoute>
                <RegisterView />
              </PublicRoute>
            }
          />
        </Route>
      </Routes>
    </Suspense>
  ) : (
    ''
  )
}

export default RoutesComponent
