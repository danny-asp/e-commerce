import { Navigate, useLocation } from 'react-router-dom'
import { useAuth } from 'core'
import { PageURLs } from 'Routes'
import React from 'react'

const AdminRoute = ({ children }) => {
  const { isAuthenticated, user } = useAuth()
  let location = useLocation()

  if (!isAuthenticated) {
    return <Navigate to={PageURLs.Login} state={{ from: location }} replace />
  } else if (isAuthenticated && user.userGroup !== 0) {
    return <Navigate to="/" state={{ from: location }} replace />
  } else {
    return children
  }
}

export default AdminRoute
