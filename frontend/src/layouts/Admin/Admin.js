import { Box, CircularProgress, Container } from '@mui/material'
import { ErrorBoundary } from 'components'
import { Outlet } from 'react-router-dom/dist'
import { Header, Sidebar } from './components'

const Admin = ({ isSuspense }) => {
  return (
    <ErrorBoundary>
      <Box sx={{ display: 'block', minHeight: '100vh', alignItems: 'center' }}>
        <Container sx={{ display: 'flex' }}>
          <Box
            sx={{
              marginTop: '1rem',
              width: '258px',
              flexShrink: '0',
              backgroundColor: '#2f65cb',
              minHeight: '95vh',
              boxShadow: (theme) => theme.shadows[14],
              borderRadius: (theme) => theme.borderRadius,
            }}
          >
            <Sidebar />
          </Box>
          <Box sx={{ width: 'calc(100% - 258px)', display: 'block' }}>
            <Box
              sx={{
                margin: '1rem 0rem 0.5rem 1rem',
                width: '98%',
                boxSizing: 'border-box',
                flexShrink: '0',
                flexDirection: 'column',
                display: 'flex',

                position: 'sticky',
                background: 'rgb(255, 255, 255)',
                zIndex: '1100',
                top: '0px',
                left: 'auto',
                right: '0px',
                borderRadius: '0px 25px 25px 0px',
                boxShadow: (theme) => theme.shadows[5],
              }}
            >
              <Header />
            </Box>
            <Box sx={{ width: '100%', height: '80%' }}>{isSuspense ? <CircularProgress /> : <Outlet />}</Box>
          </Box>
        </Container>
      </Box>
    </ErrorBoundary>
  )
}

export default Admin
