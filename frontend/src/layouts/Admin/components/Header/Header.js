import { Typography, Avatar } from '@mui/material'
import { Box } from '@mui/system'
import { Link } from 'react-router-dom'
import { useAuth } from 'core'

const Header = () => {
  const { user, logout } = useAuth()
  return (
    <Box
      component="div"
      sx={{
        width: '100%',
        height: '5rem',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: '0.2rem 1rem 1rem 1rem',
      }}
    >
      <Link to="/" style={{ maxHeight: 20 }}>
        <Box
          component="img"
          src="/images/logo.png"
          alt="logo"
          sx={{ maxHeight: 30, display: { xs: 'none', sm: 'block', md: 'block', lg: 'block' } }}
        />
      </Link>
      <Avatar alt={user?.name} sx={{ marginTop: '0.6rem' }}>
        {user?.name[0]}{' '}
      </Avatar>
    </Box>
  )
}

export default Header
