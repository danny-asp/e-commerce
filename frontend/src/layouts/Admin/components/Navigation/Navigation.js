import * as React from 'react'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import { Link } from 'react-router-dom'

const Navigation = (props) => {
  const [curentTab, setCurrentTab] = React.useState(0)
  const handleChange = (event, newValue) => {
    setCurrentTab(newValue)
  }
  return (
    <Tabs value={curentTab} onChange={handleChange} aria-label="icon label tabs example">
      <Link to="/admin">
        <Tab label="Orders" />
      </Link>

      <Link to="/admin/orders">
        <Tab label="Accounts" />
      </Link>

      <Tab label="Dashboard" />
      <Tab label={curentTab} />
    </Tabs>
  )
}

export default Navigation
