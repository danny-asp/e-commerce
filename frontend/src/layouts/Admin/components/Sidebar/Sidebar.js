import { Typography } from '@mui/material'
import { Box } from '@mui/system'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import Divider from '@mui/material/Divider'
import InboxIcon from '@mui/icons-material/Inbox'
import DraftsIcon from '@mui/icons-material/Drafts'
const Sidebar = () => {
  return (
    <Box sx={{ width: '100%', height: '5rem', display: 'block' }}>
      <Box
        sx={{
          width: '100%',
          height: '3rem',
          textAlign: 'center',
          backgroundColor: '#5e35b1',
          borderRadius: (theme) => theme.borderRadius,
        }}
      >
        <Typography variant="h2">MENU</Typography>
      </Box>
      <Box
        sx={{
          width: '100%',
          bgcolor: 'transerant',
        }}
      >
        <nav aria-label="main mailbox folders">
          <List>
            <ListItem
              disablePadding
              sx={{
                '&:hover': {
                  backgroundColor: (theme) => theme.palette.red.light,
                },
              }}
            >
              <ListItemButton>
                <ListItemIcon>
                  <InboxIcon />
                </ListItemIcon>
                <ListItemText primary="Overview" />
              </ListItemButton>
            </ListItem>
            <ListItem
              disablePadding
              sx={{
                '&:hover': {
                  backgroundColor: (theme) => theme.palette.red.light,
                },
              }}
            >
              <ListItemButton>
                <ListItemIcon>
                  <DraftsIcon />
                </ListItemIcon>
                <ListItemText primary="Orders" />
              </ListItemButton>
            </ListItem>
          </List>
        </nav>
        <Divider />
        <nav aria-label="secondary mailbox folders">
          <List>
            <ListItem
              disablePadding
              sx={{
                '&:hover': {
                  backgroundColor: (theme) => theme.palette.red.light,
                },
              }}
            >
              <ListItemButton>
                <ListItemText primary="Trash" />
              </ListItemButton>
            </ListItem>
            <ListItem
              disablePadding
              sx={{
                '&:hover': {
                  backgroundColor: (theme) => theme.palette.red.light,
                },
              }}
            >
              <ListItemButton component="a" href="/">
                <ListItemText primary="Back to Client Store" />
              </ListItemButton>
            </ListItem>
          </List>
        </nav>
      </Box>
    </Box>
  )
}

export default Sidebar
