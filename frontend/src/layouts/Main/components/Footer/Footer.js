import { Box, Container, Grid, Typography } from '@mui/material'
import { Link } from 'react-router-dom'
import PageLayout from '../PageLayout'

const Footer = () => {
  return (
    <PageLayout data="2" withContainer={false}>
      <Box
        sx={{ height: 'fit-content', bgcolor: 'primary.main', display: 'flex', alignItems: 'center', padding: '2rem' }}
      >
        <Container sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
          <Box component="div" sx={{ color: 'white', width: '100%' }}>
            <Grid container spacing={2}>
              <Grid item md={6} lg={3} sx={{ display: 'block', justifyContent: 'center' }}>
                <Link to="/" style={{ maxHeight: 20 }}>
                  <Box
                    sx={{
                      width: '10rem',
                      height: '3rem',
                      backgroundColor: 'white',
                      borderRadius: '12px',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      border: '1px solid white',
                    }}
                  >
                    <Box
                      component="img"
                      src="/images/logo.png"
                      alt="logo"
                      color="white"
                      sx={{ maxHeight: 20, display: { xs: 'none', sm: 'none', md: 'block', lg: 'block' } }}
                    />
                  </Box>
                </Link>
                <Typography sx={{ color: (theme) => theme.palette.grey[200] }}>
                  Empower your business with us, your IT partner for customer engagement and commerce.
                </Typography>
                <Box sx={{ width: '100%', height: '6rem', border: '1px solid white' }}></Box>
              </Grid>
              <Grid item md={6} lg={3} sx={{ display: 'block', justifyContent: 'center' }}>
                <Typography variant="h5">About us</Typography>
                <Typography>Carrers</Typography>
                <Typography>Our Story</Typography>
                <Typography>Terms & Conditions</Typography>
                <Typography>Privacy Policy</Typography>
              </Grid>
              <Grid
                item
                md={6}
                lg={3}
                sx={{
                  display: 'block',
                  justifyContent: 'center',
                  lineHeight: '10rem',
                  '&parent > *': {
                    margin: '3rem',
                  },
                }}
              >
                <Typography variant="h5">Customer care</Typography>
                <Typography>Help Center</Typography>
                <Typography>How to Buy</Typography>
                <Typography>Track Orders</Typography>
                <Typography>Returns & Refunds</Typography>
              </Grid>
              <Grid item md={6} lg={3} sx={{ display: 'block', justifyContent: 'center' }}>
                <Typography variant="h5">Contact us</Typography>
                <Typography>About us</Typography>
                <Typography>About us</Typography>
                <Box sx={{ width: '100%', height: '6rem', border: '1px solid white' }}></Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </Box>
    </PageLayout>
  )
}

export default Footer
