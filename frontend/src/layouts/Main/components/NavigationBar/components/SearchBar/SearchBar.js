import { Button, InputBase, Typography, Menu, MenuItem } from '@mui/material'
import SearchIcon from '@mui/icons-material/Search'

import { useState } from 'react'
import useSWR from 'swr'

import { ErrorBoundary } from 'components'

import { Box } from '@mui/system'

const SearchBar = () => {
  const [anchorEl, setAnchorEl] = useState(null)
  const open = Boolean(anchorEl)

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }

  const { data } = useSWR(`/products?page=1&productsPerPage=40`)
  let updadedArray = []
  const uniqueCategories = data?.result.filter((element) => {
    if (!updadedArray.includes(element.category)) {
      updadedArray.push(element.category)
      return element
    }
  })
  return (
    <ErrorBoundary>
      <Box
        component="div"
        sx={{
          position: 'relative',
          width: 'auto',
          maxWidth: '670px',
          height: '2.8rem',
          verticalAlign: 'top',
        }}
      >
        <Box
          sx={{
            border: '1px solid #d23f57',
            width: '500px',
            //  width to be changed afterwards!!
            height: '2.8rem',
            color: 'black',
            borderRadius: '1200px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row',
            borderColor: (theme) => theme.palette.grey[200],
            '&:hover': {
              borderColor: '#d23f57',
            },
          }}
        >
          <InputBase
            sx={{
              paddingLeft: '1rem',
              height: '2.8rem',
              borderRadius: '1200px 0px 0px 1200px',
              width: '100%',
            }}
          >
            <SearchIcon sx={{ position: 'static' }} />
          </InputBase>
          <Button
            variant="text"
            id="basic-button"
            aria-controls={open ? 'basic-menu' : undefined}
            aria-haspopup="true"
            aria-expanded={open ? 'true' : undefined}
            onClick={handleClick}
            sx={{
              width: '15rem',
              height: '100%',
              borderRadius: ' 0px 1200px 1200px 0px',
              backgroundColor: (theme) => theme.palette.grey[200],
            }}
          >
            <Box>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Typography variant="body2">All categories</Typography>
              </Box>
              <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                  'aria-labelledby': 'basic-button',
                }}
                sx={{ boxShadow: 'none' }}
              >
                {uniqueCategories?.map((product) => {
                  return (
                    <MenuItem onClick={handleClose} sx={{ boxShadow: 'none', width: '20Vh' }}>
                      {product.category}
                    </MenuItem>
                  )
                })}
              </Menu>
              {/* </div> */}
            </Box>
          </Button>
        </Box>
      </Box>
    </ErrorBoundary>
  )
}

export default SearchBar
