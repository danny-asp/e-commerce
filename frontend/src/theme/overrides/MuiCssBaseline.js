/* eslint-disable import/no-anonymous-default-export */
export default (theme) => ({
  // you can safely use all props from the default theme
  MuiCssBaseline: {
    styleOverrides: {
      body: {
        backgroundColor: '#f6f9fc',
      },
    },
  },
})
