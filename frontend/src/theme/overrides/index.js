/* eslint-disable import/no-anonymous-default-export */
import { mergeDeep } from 'utils'
import MuiButton from './MuiButton'
import MuiCssBaseline from './MuiCssBaseline'

export default function overrides(theme) {
  return mergeDeep(MuiButton(theme), MuiCssBaseline(theme))
}
