import useSWR from 'swr'

const useCategory = (categoryName) => {
  const { data: allProducts } = useSWR(`/products?page=0&productsPerPage=40`)
  allProducts?.filter((product) => {
    if (product.category === categoryName) {
      return product
    }
  })
}

export default useCategory
