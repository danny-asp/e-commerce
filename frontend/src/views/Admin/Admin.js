import { OrderList } from './components'
import React from 'react'
import { Box, Grid } from '@mui/material'

const Admin = () => {
  return (
    <Box sx={{ width: '100%', height: 'fit-content', boreder: '10px solid black' }}>
      <Grid container spacing={2} sx={{ marginTop: '1rem' }}>
        <Grid item xs={12}>
          <OrderList />
        </Grid>
        <Grid item xs={6}>
          Item
        </Grid>
        <Grid item xs={6}>
          Item
        </Grid>
        <Grid item xs={6}>
          Item
        </Grid>
      </Grid>
    </Box>
    // <div>
    //   <OrderList />
    // </div>
  )
}

export default Admin
