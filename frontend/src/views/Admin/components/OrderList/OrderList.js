import { Paper, Table, TableBody, TableCell, TableHead, TableRow, TablePagination } from '@mui/material'
import { PageLayout } from 'layouts/Main/components'
import { useState } from 'react'
import useSWR from 'swr'

const createData = (singleOrder) => {
  const incomigData = new Date(singleOrder.createdAt)
  const result = `${incomigData.getFullYear()}, ${incomigData.getMonth()}, ${incomigData.getDay()}`

  return {
    date: result,
    orderId: singleOrder._id,
    name: singleOrder.user.name,
    paymentMethod: singleOrder.payment.type,
    cart: singleOrder.cart,
  }
}

const OrderList = () => {
  const { data, error } = useSWR('/checkouts')
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(10)

  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  const rows = data?.result.map((singleOrder) => {
    return createData(singleOrder)
  })

  const rowsToShow = rows?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)

  return (
    <PageLayout data={data} error={error}>
      <Paper sx={{ width: '100%', padding: '1rem', borderRadius: (theme) => theme.borderRadius }}>
        <Table sx={{ minWidth: 750 }} size={'small'}>
          <TableHead>
            <TableRow>
              <TableCell sx={{ textAlign: 'center' }}>DATE</TableCell>
              <TableCell sx={{ textAlign: 'center' }}>NAME</TableCell>
              <TableCell sx={{ textAlign: 'center' }}>ID</TableCell>
              <TableCell sx={{ textAlign: 'center' }}>PAYMENT Method</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rowsToShow?.map((element) => {
              return (
                <TableRow>
                  <TableCell sx={{ textAlign: 'center' }}>{element.date}</TableCell>
                  <TableCell sx={{ textAlign: 'center' }}>{element.name}</TableCell>
                  <TableCell sx={{ textAlign: 'center' }}>{element.orderId}</TableCell>
                  <TableCell sx={{ textAlign: 'center' }}>{element.paymentMethod}</TableCell>
                </TableRow>
              )
            })}
          </TableBody>
        </Table>
        <TablePagination
          rowsPerPageOptions={[1, 2, 3, 4, 5, 10, 15]}
          component="div"
          count={rows?.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </PageLayout>
  )
}

export default OrderList
