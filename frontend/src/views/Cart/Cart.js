import { useCart } from 'core'
import { PageLayout } from 'layouts/Main/components'
import { CartForm, CartProduct, Details, DetailsSummary, Payment } from './components'
import { Box, Grid } from '@mui/material'
import React, { useState } from 'react'
import Stepper from '@mui/material/Stepper'
import Step from '@mui/material/Step'

import { useSnackbar } from 'notistack'

const steps = ['Cart', 'Shipping', 'Payment']

const Cart = () => {
  const { enqueueSnackbar } = useSnackbar()
  const [activeStep, setActiveStep] = React.useState(0)
  const [doneSteps, setDoneSteps] = useState([])
  const { cart } = useCart()

  const cartSnapshot = cart.map((product) => {
    return {
      product: product._id,
      quantity: product.quantity,
    }
  })

  const Spacer = (props) => {
    return (
      <Box
        sx={{
          width: '50%',
          height: '0.25rem',
          backgroundColor: (theme) => theme.palette.red.light,
          lineHeight: '21px',
        }}
      ></Box>
    )
  }

  const Bubble = (props) => {
    return (
      <Box
        sx={{
          width: '6rem',
          height: '1.3rem',
          backgroundColor:
            props.activeStep >= props.index ? (theme) => theme.palette.red.dark : (theme) => theme.palette.red.light,
          borderRadius: '16px',
          color: 'rgb(255, 255, 255)',
          cursor: 'pointer',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          fontSize: '12px',
          fontWeight: '600',
          padding: '0.5em, 1rem',
          textDecorationColor: 'rgb(255, 255, 255)',
          margin: '0.25rem 0',
          '&:hover': {
            backgroundColor: (theme) => theme.palette.red.dark,
            color: 'rgb(255, 255, 255)',
          },
        }}
      >
        <Box component="span"> {props.label} </Box>
      </Box>
    )
  }

  const handleNextStep = () => {
    if (cart.length === 0) {
      return enqueueSnackbar(`Your Shopping Cart is Empty`, { variant: 'warning' })
    }

    setDoneSteps((prevDoneSteps) => [...prevDoneSteps].concat(steps[activeStep]))
    setActiveStep((prevActiveStep) => prevActiveStep + 1)
    localStorage.setItem('cartSnapshot', JSON.stringify(cartSnapshot))
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1)
  }

  const handleReset = () => {
    setActiveStep(0)
    localStorage.removeItem('address')
    localStorage.removeItem('cartSnapshot')
    localStorage.removeItem('orderDetails')
    localStorage.removeItem('details')
    localStorage.removeItem('voucher')
  }

  return (
    <React.Fragment>
      <Box
        sx={{
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: '2rem',
        }}
      >
        <Stepper
          alternativeLable={false}
          activeStep={activeStep}
          connector={<Spacer activeStep={activeStep} />}
          sx={{ width: '50%' }}
        >
          {steps.map((label, index) => {
            return (
              <Step
                key={label}
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyCntent: 'center',
                  textAlign: 'center',
                }}
              >
                <Bubble label={label} index={index} activeStep={activeStep} />
              </Step>
            )
          })}
        </Stepper>
      </Box>

      <PageLayout isAsync={false}>
        <Grid container spacing={4}>
          <Grid item xs={12} sm={12} md={12} lg={8}>
            {activeStep === 0 && cart.map((product) => <CartProduct key={product._id} product={product} />)}
            {activeStep === 1 && <Details onBackStep={handleBack} onNextStep={handleNextStep} onReset={handleReset} />}
            {activeStep === 2 && <Payment onReset={handleReset} />}
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={4}>
            {activeStep === 0 && <CartForm onNextStep={handleNextStep} />}
            {activeStep > 0 && <DetailsSummary onNextStep={handleNextStep} />}
          </Grid>
        </Grid>
      </PageLayout>
    </React.Fragment>
  )
}

export default Cart
