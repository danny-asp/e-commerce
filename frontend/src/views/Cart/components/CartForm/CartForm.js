import { Button, Card, CssBaseline, TextareaAutosize, TextField, Typography } from '@mui/material'
import { Box } from '@mui/system'
import FormControl from '@mui/material/FormControl'
import { useCart } from 'core'
import { DisplayCurrency } from 'components'
import { useFormik } from 'formik'

const CartForm = (props) => {
  const { totalPrice } = useCart()

  const formik = useFormik({
    initialValues: {
      details: '',
      voucher: '',
    },
    onSubmit: async ({ details, voucher }) => {
      localStorage.setItem('details', JSON.stringify(details))
      localStorage.setItem('voucher', JSON.stringify(voucher))
      props.onNextStep()
    },
  })

  return (
    <Box sx={{ width: '100%', height: 'fit-content' }}>
      <Card
        sx={{
          height: '100%',
          width: '100%',
          borderRadius: '8px',
          display: 'inline-block',
          justifyContent: 'center',
          padding: '1.5rem',
        }}
      >
        <Box
          component="div"
          sx={{ display: 'flex', justifyContent: 'space-between', width: '100%', marginBottom: '3rem' }}
        >
          <Typography>total</Typography>
          <Typography>
            <DisplayCurrency number={totalPrice} />
          </Typography>
        </Box>
        <form onSubmit={formik.handleSubmit}>
          <Typography variant="body2" style={{ marginBottom: '1rem' }}>
            Additional Comments
          </Typography>

          <TextField
            multiline
            rows={4}
            fullWidth
            id="details"
            name="details"
            // label="hey"
            value={formik.values.details}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.details && Boolean(formik.errors.details)}
            helperText={formik.touched.details && formik.errors.details}
            sx={{
              mb: 2,
              '&:focus': {
                borderColor: (theme) => theme.palette.red.dark,
              },
            }}
          />
          <TextField
            placeholder="Voucher"
            fullWidth
            id="voucher"
            name="voucher"
            label="voucher"
            value={formik.values.voucher}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.voucher && Boolean(formik.errors.voucher)}
            helperText={formik.touched.voucher && formik.errors.voucher}
            sx={{ mb: 2 }}
          ></TextField>
          <Button
            variant="contained"
            sx={{
              width: '100%',
              marginTop: '1rem',
              boxShadow: 'none',
              color: (theme) => theme.palette.red.dark,
              backgroundColor: (theme) => theme.palette.grey[100],
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: (theme) => theme.palette.red.light,
                borderColor: (theme) => theme.palette.red.dark,
              },
            }}
          >
            Apply voucher
          </Button>

          <Button
            // onClick={props.onNextStep}
            variant="contained"
            type="submit"
            sx={{
              width: '100%',
              marginTop: '1rem',
              boxShadow: 'none',
              color: (theme) => theme.palette.red.dark,
              backgroundColor: (theme) => theme.palette.grey[100],
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: (theme) => theme.palette.red.light,
                borderColor: (theme) => theme.palette.red.dark,
              },
            }}
          >
            Checkout Now
          </Button>
        </form>
        {/* <FormControl sx={{ width: '100%', height: 'fit-content' }}>
          <Typography variant="body2" style={{ marginBottom: '1rem' }}>
            Additional Comments
          </Typography>

          <TextField id="standard-multiline-static" multiline rows={4} style={{ marginBottom: '2rem' }} />
          <TextField id="outlined-textarea" label="Voucher" placeholder="Voucher" multiline></TextField>
          <Button
            variant="contained"
            sx={{
              boxShadow: 'none',
              color: (theme) => theme.palette.red.dark,
              backgroundColor: (theme) => theme.palette.grey[100],
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: (theme) => theme.palette.red.light,
                borderColor: (theme) => theme.palette.red.dark,
              },
            }}
          >
            Apply voucher
          </Button>

          <Typography variant="body2" style={{ marginBottom: '1rem' }}>
            Shipping
          </Typography>
          <Button
            onClick={props.onNextStep}
            variant="contained"
            sx={{
              boxShadow: 'none',
              color: (theme) => theme.palette.red.dark,
              backgroundColor: (theme) => theme.palette.grey[100],
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: (theme) => theme.palette.red.light,
                borderColor: (theme) => theme.palette.red.dark,
              },
            }}
          >
            Checkout Now
          </Button>

          <CssBaseline></CssBaseline>
        </FormControl> */}
      </Card>
    </Box>
  )
}

export default CartForm
