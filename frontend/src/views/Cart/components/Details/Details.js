import React from 'react'
import * as yup from 'yup'
import { useFormik } from 'formik'
import { TextField, Box, Button, Grid } from '@mui/material'
import { PageLayout } from 'layouts/Main/components'

const Details = (props) => {
  const itemFromStorae = JSON.parse(localStorage.address ?? null)

  const fullNameRegExp = /[A-Za-z]/
  const phoneRegExp =
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
  const zipCodeRegExp = /^[0-9]+$/
  const emailRegExp =
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/

  const validationSchema = yup.object({
    fullName: yup
      .string('Enter your full name')
      .matches(fullNameRegExp, 'Only letters allowed')
      .required('Full name is required'),
    phoneNumber: yup.string().matches(phoneRegExp, 'Phone number is not valid').required('Phone number is required'),
    zipCode: yup
      .string('Enter your zip code')
      .required('Zip code is required')
      .matches(zipCodeRegExp, 'invalid Zip Code')
      .min(4, 'four digit zip code required')
      .max(4, 'four digit zip code required'),

    address1: yup.string('Enter your address').required('Address is required').min(10, 'minimum of 10 symbols'),

    email: yup.string('Enter your email').email('Enter a valid email').required('Email is required'),
    // .matches(emailRegExp, 'enter valid email'),
    country: yup.string('Enter your country').required('Country is required'),
    address2: yup.string('Enter your address'),
  })

  const formik = useFormik({
    initialValues: {
      fullName: itemFromStorae ? itemFromStorae.fullName : '',
      phoneNumber: itemFromStorae ? itemFromStorae.phone : '',
      zipCode: itemFromStorae ? itemFromStorae.zip : '',
      address1: itemFromStorae ? itemFromStorae.address1 : '',
      email: itemFromStorae ? itemFromStorae.email : '',
      country: itemFromStorae ? itemFromStorae.country : '',
      address2: itemFromStorae ? itemFromStorae.address2 : '',
    },
    validationSchema,
    onSubmit: async ({ fullName, phoneNumber, zipCode, address1, email, country, address2 }) => {
      const address = {
        fullName: fullName,
        phone: phoneNumber,
        email: email,
        zip: zipCode,
        address1: address1,
        address2: address2, // not required
        country: country,
      }
      props.onNextStep()
      localStorage.setItem('address', JSON.stringify(address))
    },
  })

  return (
    <React.Fragment>
      <PageLayout isAsync={false} withContainer={false}>
        <Box
          sx={{
            width: '100%',
            height: '43rem',
            backgroundColor: 'white',
            borderRadius: '12px',
            padding: '3rem',
          }}
        >
          <form onSubmit={formik.handleSubmit}>
            <Grid container spacing={0}>
              <Grid item xs={12} sm={6} md={6} sx={{ width: '100%', padding: '1rem' }}>
                <TextField
                  fullWidth
                  id="fullName"
                  label="Full Name"
                  name="fullName"
                  value={formik.values.fullName}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.fullName && Boolean(formik.errors.fullName)}
                  helperText={formik.touched.fullName && formik.errors.fullName}
                  sx={{ mb: 2 }}
                />
                <TextField
                  fullWidth
                  id="phoneNumber"
                  name="phoneNumber"
                  label="Phone Number"
                  value={formik.values.phoneNumber}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.phoneNumber && Boolean(formik.errors.phoneNumber)}
                  helperText={formik.touched.phoneNumber && formik.errors.phoneNumber}
                  sx={{ mb: 2 }}
                />
                <TextField
                  fullWidth
                  id="zipCode"
                  name="zipCode"
                  label="Zip Code"
                  value={formik.values.zipCode}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.zipCode && Boolean(formik.errors.zipCode)}
                  helperText={formik.touched.zipCode && formik.errors.zipCode}
                  sx={{ mb: 2 }}
                />
                <TextField
                  fullWidth
                  id="address1"
                  name="address1"
                  label="Address 1 "
                  value={formik.values.address1}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.address1 && Boolean(formik.errors.address1)}
                  helperText={formik.touched.address1 && formik.errors.address1}
                  sx={{ mb: 2 }}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} sx={{ width: '100%', padding: '1rem' }}>
                <TextField
                  fullWidth
                  id="email"
                  label="Email"
                  name="email"
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.email && Boolean(formik.errors.email)}
                  helperText={formik.touched.email && formik.errors.email}
                  sx={{ mb: 2 }}
                />
                <TextField
                  fullWidth
                  id="country"
                  label="Country"
                  name="country"
                  value={formik.values.country}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.country && Boolean(formik.errors.country)}
                  helperText={formik.touched.country && formik.errors.country}
                  sx={{ mb: 2 }}
                />
                <TextField
                  fullWidth
                  id="address2"
                  label="Address 2"
                  name="address2"
                  value={formik.values.address2}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.address2 && Boolean(formik.errors.address2)}
                  helperText={formik.touched.address2 && formik.errors.address2}
                  sx={{ mb: 2 }}
                />
                <Button
                  color="primary"
                  variant="contained"
                  type="button"
                  sx={{ width: '100%' }}
                  onClick={props.onReset}
                >
                  Reset
                </Button>
              </Grid>
            </Grid>
            <Grid container spacing={0}>
              <Grid item xs={12} sm={6} md={6} sx={{ width: '100%', padding: '1rem' }}>
                <Button
                  color="primary"
                  variant="contained"
                  type="button"
                  sx={{ width: '100%' }}
                  onClick={props.onBackStep}
                >
                  Back
                </Button>
              </Grid>
              <Grid item xs={12} sm={6} md={6} sx={{ width: '100%', padding: '1rem' }}>
                <Button color="primary" variant="contained" type="submit" sx={{ width: '100%' }}>
                  Submit
                </Button>
              </Grid>
            </Grid>
          </form>
        </Box>
      </PageLayout>
    </React.Fragment>
  )
}

export default Details
