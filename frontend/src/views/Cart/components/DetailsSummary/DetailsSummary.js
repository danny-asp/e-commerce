import { Paper, Box, Typography, CssBaseline } from '@mui/material'
import { DisplayCurrency } from 'components'
import { useCart } from 'core'

const DetailsSummary = () => {
  const { totalPrice } = useCart()
  return (
    <Paper
      sx={{
        borderRadius: (theme) => theme.borderRadius,
        transition: 'box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
        boxShadow: (theme) => theme.boxShadow,
        marginBottom: '2rem',
        display: 'block',
        width: '100%',
        padding: '1rem',
      }}
    >
      <Box
        sx={{
          width: '100%',
          marginBottom: '0.6rem',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        <Typography variant="body2">Subtotal</Typography>
        <Typography variant="h4">
          <DisplayCurrency number={totalPrice} />
        </Typography>
      </Box>
      <Box
        sx={{
          width: '100%',
          marginBottom: '0.6rem',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        <Typography variant="body2">Shipping</Typography>
        <Typography variant="h4">
          <DisplayCurrency number="10" />
        </Typography>
      </Box>
      <Box
        sx={{
          width: '100%',
          marginBottom: '0.6rem',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        <Typography variant="body2">Tax</Typography>
        <Typography variant="h4">
          <DisplayCurrency number="0" />
        </Typography>
      </Box>
      <Box
        sx={{
          width: '100%',
          marginBottom: '0.6rem',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        <Typography variant="body2">Discount</Typography>
        <Typography variant="h4">
          <DisplayCurrency number="0" />
        </Typography>
      </Box>
      <Box sx={{ height: '2px', width: '100%', backgroundColor: 'red' }}>
        <CssBaseline />
      </Box>
      <Box
        sx={{
          width: '100%',
          marginBottom: '0.6rem',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginTop: '2rem',
        }}
      >
        <Typography variant="body2"></Typography>
        <Typography variant="h4">
          <DisplayCurrency number={totalPrice + 10} />
        </Typography>
      </Box>
    </Paper>
  )
}

export default DetailsSummary
