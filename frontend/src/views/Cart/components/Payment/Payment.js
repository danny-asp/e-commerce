import React from 'react'
import { PageLayout } from 'layouts/Main/components'
import * as yup from 'yup'
import { useFormik } from 'formik'
import Accordion from '@mui/material/Accordion'
import AccordionSummary from '@mui/material/AccordionSummary'
import AccordionDetails from '@mui/material/AccordionDetails'
import Typography from '@mui/material/Typography'
import { TextField, Box, Button, Grid } from '@mui/material'
import Radio from '@mui/material/Radio'
import RadioGroup from '@mui/material/RadioGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'

const Payment = (props) => {
  const [paymentMethod, setPaymentMethod] = React.useState('card')
  const navigate = useNavigate()
  const cardNumberRegExp =
    /^(?:(4[0-9]{12}(?:[0-9]{3})?)|(5[1-5][0-9]{14})|(6(?:011|5[0-9]{2})[0-9]{12})|(3[47][0-9]{13})|(3(?:0[0-5]|[68][0-9])[0-9]{11})|((?:2131|1800|35[0-9]{3})[0-9]{11}))$/
  const cardNameRegExp = /[A-Za-z]/
  const cardExDateRegExp = /([0-9]{4})\/([0-9]{2})/

  const validationSchema = yup.object({
    cardNumber: yup
      .string('Enter your credit card number')
      .matches(cardNumberRegExp, 'Please, enter valid Card Number')
      .required('Card Number is required'),
    expiryDate: yup
      // .date()
      // .transform(function (value, originalValue) {
      //   if (this.isType(value)) {
      //     return value
      //   }
      //   const result = new Date(originalValue)
      //   console.log(result)
      //   return result
      // })
      // .typeError('please enter a valid date')
      // .required(),
      .number(),

    cardName: yup
      .string('Enter your card Name')
      .required('Card Name is required')
      .matches(cardNameRegExp, 'invalid Card Name'),
    ccvCode: yup.string('Enter your card CCV code').required('CCV code is required').min(3).max(4),
  })

  const formik = useFormik({
    initialValues: {
      cardNumber: '',
      expiryDate: '',
      cardName: '',
      ccvCode: '',
    },
    validationSchema,
    onSubmit: async ({ cardNumber, expiryDate, cardName, ccvCode }) => {
      console.log(expiryDate)
      const paymentDetails = {
        type: paymentMethod,
        card: {
          name: cardName,
          ccv: ccvCode,
          number: cardNumber,
          expiryDate: new Date(expiryDate),
        },
        details: '',
      }

      await axios
        .post('http://localhost:5000/api/checkouts/create', {
          cart: await JSON.parse(localStorage.cartSnapshot),
          address: await JSON.parse(localStorage.address),
          details: await JSON.parse(localStorage.details),
          payment: paymentDetails,
        })
        .then(
          (response) => {
            const orderDetails = response.data
            localStorage.setItem('orderDetails', JSON.stringify(orderDetails))
            console.log(response.data)
            navigate(`/orders/${response?.data.id}`)
          },
          (error) => {
            console.log(error)
          }
        )
    },
  })

  const handleChange = (value) => {
    setPaymentMethod(value.target.value)
  }
  return (
    <React.Fragment>
      <PageLayout isAsync={false} withContainer={false}>
        <Box
          sx={{
            width: '100%',
            height: 'fit-content',
            backgroundColor: 'white',
            borderRadius: '12px',
          }}
        >
          <RadioGroup
            aria-labelledby="demo-radio-buttons-group-label"
            defaultValue="credit-card"
            name="radio-buttons-group"
            onChange={handleChange}
          >
            <Accordion sx={{ boxShadow: 'none', margin: '0' }}>
              <AccordionSummary aria-controls="panel2a-content" id="panel2a-header">
                <FormControlLabel value="card" control={<Radio />} label="Pay With Credit Card" />
              </AccordionSummary>
              <AccordionDetails>
                <Typography>
                  <form onSubmit={formik.handleSubmit}>
                    <Grid container spacing={0}>
                      <Grid item xs={12} sm={6} md={6} sx={{ width: '100%', padding: '1rem' }}>
                        <TextField
                          fullWidth
                          id="cardName"
                          label="Card Name"
                          name="cardName"
                          value={formik.values.cardName}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          error={formik.touched.cardName && Boolean(formik.errors.cardName)}
                          helperText={formik.touched.cardName && formik.errors.cardName}
                          sx={{ mb: 2 }}
                        />
                        <TextField
                          fullWidth
                          id="cardNumber"
                          label="Card Number"
                          name="cardNumber"
                          value={formik.values.cardNumber}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          error={formik.touched.cardNumber && Boolean(formik.errors.cardNumber)}
                          helperText={formik.touched.cardNumber && formik.errors.cardNumber}
                          sx={{ mb: 2 }}
                        />
                      </Grid>
                      <Grid item xs={12} sm={6} md={6} sx={{ width: '100%', padding: '1rem' }}>
                        <TextField
                          fullWidth
                          id="ccvCode"
                          label="CCV Code"
                          name="ccvCode"
                          value={formik.values.ccvCode}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          error={formik.touched.ccvCode && Boolean(formik.errors.ccvCode)}
                          helperText={formik.touched.ccvCode && formik.errors.ccvCode}
                          sx={{ mb: 2 }}
                        />
                        <TextField
                          fullWidth
                          id="expiryDate"
                          label="Exp Date"
                          placeholder="MM/YY"
                          name="expiryDate"
                          value={formik.values.expiryDate}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          error={formik.touched.expiryDate && Boolean(formik.errors.expiryDate)}
                          helperText={formik.touched.expiryDate && formik.errors.expiryDate}
                          sx={{ mb: 2 }}
                        />
                      </Grid>
                    </Grid>
                    <Button color="primary" variant="contained" type="submit" sx={{ width: '100%' }}>
                      Send the order
                    </Button>
                  </form>
                </Typography>
              </AccordionDetails>
            </Accordion>
            <Accordion sx={{ boxShadow: 'none', margin: '0' }}>
              <AccordionSummary aria-controls="panel2a-content">
                <FormControlLabel value="paypal" control={<Radio />} label="Pay With Paypal" id="panel2a-header" />
              </AccordionSummary>
              <AccordionDetails>
                <Typography>email</Typography>
                <Button color="primary" variant="contained" type="submit" sx={{ width: '100%' }}>
                  Send the order
                </Button>
              </AccordionDetails>
            </Accordion>
            <Accordion sx={{ boxShadow: 'none', margin: '0' }}>
              <AccordionSummary aria-controls="panel2a-content">
                <FormControlLabel value="cash" control={<Radio />} label="Cash on Delivery" id="panel2a-header" />
              </AccordionSummary>
              <AccordionDetails>
                <Typography>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet
                  blandit leo lobortis eget.
                </Typography>
              </AccordionDetails>
            </Accordion>
          </RadioGroup>
        </Box>
      </PageLayout>
    </React.Fragment>
  )
}

export default Payment
