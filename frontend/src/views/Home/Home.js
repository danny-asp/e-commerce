import React from 'react'
import { BigDiscounts, Categories, DisplayCategory, ProductList, Support } from './components'
import { HeroSwiper } from './components'
import { FlashDeals } from './components'
import { TopCategories } from './components'
import { TopRating } from './components'
import { NewArrivals } from './components'

const Home = () => {
  return (
    <React.Fragment>
      <HeroSwiper />
      <FlashDeals />
      <TopCategories />
      <TopRating />
      <NewArrivals />
      <BigDiscounts />
      <DisplayCategory category="laptops" />
      <DisplayCategory category="smartphones" />
      <Categories />

      <ProductList />

      <Support />
    </React.Fragment>
  )
}

export default Home
