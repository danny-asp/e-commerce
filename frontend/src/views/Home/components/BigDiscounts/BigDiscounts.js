import { Box, Typography, Card, CardMedia, CardContent } from '@mui/material'
import { PageLayout } from 'layouts/Main/components'
import CakeIcon from '@mui/icons-material/Cake'

import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/pagination'
import 'swiper/css/navigation'
import { Pagination, Navigation, Scrollbar, A11y } from 'swiper'
import useSWR from 'swr'
import { DisplayCurrency } from 'components'
import { Link } from 'react-router-dom'

const BigDiscounts = () => {
  const { data: products } = useSWR(`/products?page=3&productsPerPage=10`)

  return (
    <PageLayout data="2">
      <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', marginBottom: '1rem' }}>
        <Box component="span" sx={{ display: 'flex', alignItems: 'center' }}>
          <CakeIcon sx={{ color: '#D23F57' }} />
          <Typography variant="h5" sx={{ marginLeft: '0.5rem', fontWeight: '600' }}>
            Big Discounts
          </Typography>
        </Box>
        <Box component="span">view all</Box>
      </Box>

      <Box>
        <Swiper
          modules={[Navigation, Pagination, Scrollbar, A11y]}
          spaceBetween={20}
          slidesPerView={1}
          breakpoints={{
            500: { slidesPerView: 4 },
            700: { slidesPerView: 6 },
            900: { slidesPerView: 6 },
          }}
          navigation={{ nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' }}
          pagination={false}
          scrollbar={{ draggable: true }}
          onSwiper={(swiper) => console.log(swiper)}
          onSlideChange={() => console.log('slide change')}
        >
          <Box
            class="swiper-button-prev"
            sx={{
              width: '2rem',
              heigth: '2rem',
              border: '1px solid black',
            }}
          ></Box>
          <Box component="div" class="swiper-button-next" width="0.2rem" height="2rem" sx={{ width: '10px' }}></Box>

          {products?.result.map((product) => {
            return (
              <SwiperSlide key={product._id}>
                <Link to={`/products/${product._id}`}>
                  <Card sx={{ height: '100%', width: '100%', borderRadius: '8px' }}>
                    <Box sx={{ padding: '1.5rem', borderRadius: '8px' }}>
                      <CardMedia
                        component="img"
                        height="100"
                        image={product.thumbnail}
                        alt={product.title}
                        sx={{
                          borderRadius: '8px',
                          '&:hover': {
                            filter: 'brightness(50%)',
                          },
                        }}
                      />
                    </Box>

                    <CardContent sx={{ paddingTop: '0' }}>
                      <Typography gutterBottom variant="body" component="div" noWrap>
                        {product.title}
                      </Typography>
                      <Box display="flex" justifyContent="space-between" alignItems="center" sx={{ width: '100%' }}>
                        <Typography variant="body2" component="div">
                          {product.discountPercentage > 0 ? (
                            <>
                              <Box display="flex" alignItems="center">
                                <Box>
                                  <DisplayCurrency
                                    number={product.price - (product.price / 100) * product.discountPercentage}
                                  />
                                </Box>
                                <Box sx={{ textDecoration: 'line-through', ml: 1, color: 'error.main' }}>
                                  <DisplayCurrency number={product.price} />
                                </Box>
                              </Box>
                            </>
                          ) : (
                            <DisplayCurrency number={product.price} />
                          )}
                        </Typography>
                      </Box>
                    </CardContent>
                  </Card>
                </Link>
              </SwiperSlide>
            )
          })}
        </Swiper>
      </Box>
    </PageLayout>
  )
}

export default BigDiscounts
