import React, { Component } from 'react'
import Slider from 'react-slick'
// import '~slick-carousel/slick/slick.css'
// import '~slick-carousel/slick/slick-theme.css'

export default class MultipleItems extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 3,
    }
    return (
      <div>
        <h2> Multiple items </h2>
        <Slider {...settings}>
          <div>
            <h3>123123123123</h3>
          </div>
          <div>
            <h3>2123123123123</h3>
          </div>
          <div>
            <h3>312312312312312</h3>
          </div>
          <div>
            <h3>4123123123123</h3>
          </div>
          <div>
            <h3>5123123123123</h3>
          </div>
          <div>
            <h3>1233333333333333333333333333333333333333336</h3>
          </div>
          <div>
            <h3>733333333333333333333333333333333</h3>
          </div>
          <div>
            <h3>8333333333333333333333333333333333333</h3>
          </div>
          <div>
            <h3>9333333333333333333333333333333333333333333333333</h3>
          </div>
        </Slider>
      </div>
    )
  }
}
