import { Box, CardMedia, Typography } from '@mui/material'
import { PageLayout } from 'layouts/Main/components'
import AppsIcon from '@mui/icons-material/Apps'
import { Card, Grid } from '@mui/material'
import useSWR from 'swr'
import { Link } from 'react-router-dom'

const Categories = () => {
  const { data } = useSWR(`/products?page=1&productsPerPage=40`)
  let updadedArray = []
  const uniqueCategories = data?.result.filter((element) => {
    if (!updadedArray.includes(element.category)) {
      updadedArray.push(element.category)
      return element
    }
  })

  return (
    <PageLayout data="2">
      <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', marginBottom: '1rem' }}>
        <Box component="span" sx={{ display: 'flex', alignItems: 'center' }}>
          <AppsIcon sx={{ color: '#D23F57' }} />
          <Typography variant="h5" sx={{ marginLeft: '0.5rem', fontWeight: '600' }}>
            Categories
          </Typography>
        </Box>
        <Box component="span">view all</Box>
      </Box>
      <Grid container spacing={3}>
        {uniqueCategories?.map((product) => {
          return (
            <Grid item xs={12} sm={4} md={3} lg={2} key={product._id}>
              <Link to={`/products/${product.category}`}>
                <Card
                  sx={{
                    height: '4.23rem',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    boxShadow: (theme) => theme.shadows[2],
                    '&:hover': {
                      boxShadow: (theme) => theme.shadows[15],
                    },
                    borderRadius: '8px',
                    transition: 'all 250ms ease-in-out',
                    padding: '1rem',
                  }}
                >
                  <CardMedia
                    component="img"
                    height="40"
                    image={product.thumbnail}
                    alt={product.title}
                    sx={{ borderRadius: '8px' }}
                  />

                  <Typography sx={{ marginLeft: '1rem', width: '160%' }}>{product.category}</Typography>
                </Card>
              </Link>
            </Grid>
          )
        })}
      </Grid>
    </PageLayout>
  )
}

export default Categories
