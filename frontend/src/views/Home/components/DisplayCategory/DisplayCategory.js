import { Box, Typography, Rating, IconButton, CardMedia, CardContent } from '@mui/material'
import { PageLayout } from 'layouts/Main/components'
import CakeIcon from '@mui/icons-material/Cake'
import { Card, Grid } from '@mui/material'
import useSWR from 'swr'
import { useState } from 'react'
import { DisplayCurrency } from 'components'
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart'
import { useCart } from 'core'
import { Link } from 'react-router-dom'

const DisplayCategory = (props) => {
  const [displayProducts, setDisplayProducts] = useState([])
  const { addToCart } = useCart()
  const currentCategory = props.category
  const { data } = useSWR(`/products?page=1&productsPerPage=40`)
  const allProductsFromCategory = data?.result.filter((element) => {
    return element.category === currentCategory
  })

  const onSelectBrandAction = (event) => {
    setDisplayProducts(event.target.textContent)
  }

  return (
    <PageLayout data="2">
      <Grid container spacing={3}>
        <Grid item xs={0} md={3} sx={{ display: { xs: 'none', lg: 'flex', xl: 'flex' } }}>
          <Box>
            <Card height="200" width="10rem">
              <Box
                sx={{
                  width: '14rem',
                  height: '30rem',
                  display: 'block',
                  alignItems: 'center',
                  textAlign: 'center',
                  padding: '1rem',
                }}
              >
                <Box sx={{ marginBottom: '2rem' }}>
                  <Typography>Brands</Typography>
                </Box>
                {allProductsFromCategory?.map((element) => {
                  return (
                    <Card
                      sx={{
                        marginBottom: '1rem',
                        backgroundColor: '#F6F9FC',

                        boxShadow: (theme) => theme.shadows[0],
                        '&:hover': {
                          opacity: '1',
                          backgroundColor: 'white',
                          boxShadow: (theme) => theme.shadows[10],
                          cursor: 'pointer',
                        },
                      }}
                      onClick={onSelectBrandAction}
                    >
                      <Typography>{element.brand}</Typography>
                    </Card>
                  )
                })}
              </Box>
            </Card>
          </Box>
        </Grid>

        <Grid item xs={12} md={12} lg={9}>
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: '1rem',
            }}
          >
            <Box component="span" sx={{ display: 'flex', alignItems: 'center' }}>
              <CakeIcon sx={{ color: '#D23F57' }} />
              <Typography variant="h5" sx={{ marginLeft: '0.5rem', fontWeight: '600' }}>
                {props.category}
              </Typography>
            </Box>
            <Box component="span">view all</Box>
          </Box>
          <Grid container spacing={3}>
            {allProductsFromCategory?.map((product) => {
              return (
                <Grid item xs={6} md={4}>
                  <Card
                    sx={{
                      height: '100%',
                      width: '100%',
                      borderRadius: '12px',
                      boxShadow: (theme) => theme.shadows[4],
                      '&:hover': {
                        boxShadow: (theme) => theme.shadows[15],
                      },
                    }}
                  >
                    <Link to={`/products/${product._id}`}>
                      <CardMedia component="img" height="240" image={product.thumbnail} alt={product.title} />
                    </Link>
                    <CardContent>
                      <Typography gutterBottom variant="body" component="div" noWrap>
                        {product.title}
                      </Typography>
                      <Rating name="read-only" value={product.rating} readOnly size="small" />

                      <Box display="flex" justifyContent="space-between" alignItems="center" sx={{ width: '100%' }}>
                        <Typography variant="body2" component="div">
                          {product.discountPercentage > 0 ? (
                            <>
                              <Box display="flex" alignItems="center">
                                <Box>
                                  <DisplayCurrency
                                    number={product.price - (product.price / 100) * product.discountPercentage}
                                  />
                                </Box>
                                <Box sx={{ textDecoration: 'line-through', ml: 1, color: 'error.main' }}>
                                  <DisplayCurrency number={product.price} />
                                </Box>
                              </Box>
                            </>
                          ) : (
                            <DisplayCurrency number={product.price} />
                          )}
                        </Typography>
                        <IconButton color="primary" onClick={() => addToCart(product)}>
                          <AddShoppingCartIcon />
                        </IconButton>
                      </Box>
                    </CardContent>
                  </Card>
                </Grid>
              )
            })}
          </Grid>
        </Grid>
      </Grid>
    </PageLayout>
  )
}

export default DisplayCategory
