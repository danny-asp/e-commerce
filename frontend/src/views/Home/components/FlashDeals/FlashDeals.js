import { Box, Typography } from '@mui/material'
import { PageLayout } from 'layouts/Main/components'
import ElectricBoltIcon from '@mui/icons-material/ElectricBolt'
import { Card, CardMedia, CardContent, Rating, IconButton } from '@mui/material'
import useSWR from 'swr'
import { DisplayCurrency } from 'components'
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart'

import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/pagination'
import 'swiper/css/navigation'
import { Pagination, Navigation, Scrollbar, A11y } from 'swiper'

import { useCart } from 'core'
import { Link } from 'react-router-dom'

const FlashDeals = () => {
  const { data: products, error } = useSWR(`/products?page=4&productsPerPage=8`)
  const { addToCart } = useCart()

  return (
    <PageLayout data="2" error={error}>
      <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', marginBottom: '1rem' }}>
        <Box component="span" sx={{ display: 'flex', alignItems: 'center' }}>
          <ElectricBoltIcon sx={{ color: '#D23F57' }} />
          <Typography variant="h4" sx={{ marginLeft: '0.5rem', fontWeight: '600' }}>
            Flash Deals
          </Typography>
        </Box>
        <Box component="span">view all</Box>
      </Box>
      <Box>
        {/* <Button
          sx={{
            width: '30px',
            height: '60px',
            backgroundColor: 'black',
            borderRadius: '50%',
            position: 'reletive',
            left: '-2rem',
            top: '13rem',
            zIndex: '1500',
            '&:hover': {
              backgroundColor: 'black',
            },
          }}
        ></Button> */}
        <Swiper
          loop={true}
          modules={[Navigation, Pagination, Scrollbar, A11y]}
          spaceBetween={25}
          slidesPerView={1}
          breakpoints={{
            500: { slidesPerView: 2 },
            700: { slidesPerView: 3 },
            900: { slidesPerView: 4 },
          }}
          navigation={{ nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' }}
          pagination={false}
          scrollbar={{ draggable: true }}
          onSwiper={(swiper) => console.log(swiper)}
          onSlideChange={() => console.log('slide change')}
        >
          <Box component="div" class="swiper-button-prev" width="0.2rem" height="2rem" sx={{ width: '10px' }}></Box>
          <Box
            component="div"
            class="swiper-button-next"
            width="0.2rem"
            height="2rem"
            position="relative"
            right="2rem"
            sx={{ width: '10px', backgroundColor: 'black' }}
          ></Box>

          {/* <NextPlanIcon class="swiper-button-next" sx={{ position: 'reletive', right: '20rem', fontSize: '3rem' }} /> */}

          {products?.result.map((product) => {
            return (
              <SwiperSlide key={product._id}>
                <Card sx={{ height: '100%', width: '100%', borderRadius: '8px' }}>
                  <Box
                    sx={{
                      position: 'absolute',
                      top: '10px',
                      left: '10px',
                      width: '52px',
                      height: '24px',
                      backgroundColor: '#D23F57',
                      textAlign: 'center',
                      borderRadius: '16px',
                      display: 'inline-flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      fontSize: '0.6rem',
                      fontWeight: '600',
                      color: 'white',
                    }}
                  >
                    {`${Math.trunc(product.discountPercentage)}% off`}
                  </Box>
                  <Link to={`/products/${product._id}`} sx={{ textDecoration: 'none' }}>
                    <CardMedia component="img" height="240" image={product.thumbnail} alt={product.title} />
                  </Link>
                  <CardContent>
                    <Link to={`/products/${product._id}`} sx={{ textDecoration: 'none' }}>
                      <Typography
                        gutterBottom
                        variant="h6"
                        noWrap
                        sx={{ textDecoration: 'none', textDecorationLine: 'none' }}
                      >
                        {product.title}
                      </Typography>
                    </Link>
                    <Rating name="read-only" value={product.rating} readOnly size="small" />

                    <Box display="flex" justifyContent="space-between" alignItems="center" sx={{ width: '100%' }}>
                      <Typography variant="body2" component="div">
                        {product.discountPercentage > 0 ? (
                          <>
                            <Box display="flex" alignItems="center">
                              <Box>
                                <DisplayCurrency
                                  number={product.price - (product.price / 100) * product.discountPercentage}
                                />
                              </Box>
                              <Box sx={{ textDecoration: 'line-through', ml: 1, color: 'error.main' }}>
                                <DisplayCurrency number={product.price} />
                              </Box>
                            </Box>
                          </>
                        ) : (
                          <DisplayCurrency number={product.price} />
                        )}
                      </Typography>
                      <IconButton color="primary" onClick={() => addToCart(product)}>
                        <AddShoppingCartIcon />
                      </IconButton>
                    </Box>
                  </CardContent>
                </Card>
              </SwiperSlide>
            )
          })}
        </Swiper>
      </Box>
    </PageLayout>
  )
}

export default FlashDeals
