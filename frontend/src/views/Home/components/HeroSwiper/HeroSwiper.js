import { Box } from '@mui/system'
import { Grid, Paper, CardMedia, Typography, Button } from '@mui/material'

import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/pagination'
// import './styles.css'
import { Pagination } from 'swiper'
import useSWR from 'swr'
import { queryParamAsArray } from 'utils'

const HeroSwiper = () => {
  const highlightedProductsId = ['636123466a11d03ec9d9a1be', '636123466a11d03ec9d9a1cf']

  const { data } = useSWR(
    highlightedProductsId.length ? `/products/by-ids?${queryParamAsArray('productIds', highlightedProductsId)}` : null
  )

  return (
    // <PageLayout data="2" withContainer={false}>
    <Paper
      sx={{
        width: '100%',
        height: 'fit-content',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        boxShadow: 'none',
        padding: '4rem',
        marginBottom: '5rem',
      }}
    >
      <Box
        sx={{
          // border: '1px solid black',
          width: '100%',
          height: '100%',
          maxWidth: '1152px',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Box sx={{ width: '100%', height: '100%' }}>
          <Swiper
            spaceBetween={30}
            pagination={{
              clickable: true,
            }}
            modules={[Pagination]}
          >
            {data?.result.map((element) => {
              return (
                <SwiperSlide key={element._id}>
                  <Box component="div" sx={{ width: '100%', height: 'fit-content' }}>
                    <Grid container spacing={3}>
                      <Grid item sm={12} md={6}>
                        <Box
                          component="div"
                          sx={{ width: '100%', height: '22rem', padding: '3rem ', marginTop: '1rem' }}
                        >
                          <Typography variant="h3" component="h1" sx={{ fontWeight: '700', marginBottom: '2rem' }}>
                            50% Off For Your First Shopping
                          </Typography>
                          <Typography variant="body2" sx={{ marginBottom: '2rem' }}>
                            Random textRandom textRandom textRandom textRandom textRandom textRandom textRandom
                            textRandom text
                          </Typography>
                          <Button
                            variant="contained"
                            sx={{
                              marginTop: '2rem',
                              boxShadow: 'none',
                              color: (theme) => theme.palette.grey[100],
                              backgroundColor: (theme) => theme.palette.red.dark,
                              filter: 'brightness(90%)',
                              '&:hover': {
                                boxShadow: 'none',
                                backgroundColor: (theme) => theme.palette.red.dark,
                                filter: 'brightness(100%)',
                              },
                            }}
                          >
                            Shop Now!
                          </Button>
                        </Box>
                      </Grid>
                      <Grid item sm={12} md={6}>
                        <Box component="div" sx={{ width: '100%', height: '22rem', padding: '2rem ' }}>
                          <CardMedia
                            component="img"
                            height="100%"
                            image={element.thumbnail}
                            alt={element.title}
                            sx={{ padding: '2rem 0rem 0 2rem' }}
                          />
                        </Box>
                      </Grid>
                    </Grid>
                  </Box>
                </SwiperSlide>
              )
            })}
          </Swiper>
        </Box>
      </Box>
    </Paper>
    // </PageLayout>
  )
}

export default HeroSwiper
