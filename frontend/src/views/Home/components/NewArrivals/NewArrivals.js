import { Box, Card, CardMedia, Grid, Paper, Typography } from '@mui/material'
import { PageLayout } from 'layouts/Main/components'
import FiberNewIcon from '@mui/icons-material/FiberNew'
import useSWR from 'swr'
import { DisplayCurrency } from 'components'
import { Link } from 'react-router-dom'

const NewArrivals = () => {
  const { data: products, error } = useSWR(`/products?page=2&productsPerPage=6`)

  return (
    <PageLayout data="2" error={error}>
      <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
        <Box component="div" sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', gap: '8px' }}>
          <Box paddingBottom="0.5rem">
            <FiberNewIcon sx={{ color: '#D23F57' }} />
          </Box>
          <Box>
            <Typography variant="h5" marginBottom="1rem" fontWeight="700">
              New Arrivals
            </Typography>
          </Box>
        </Box>
        <Box component="span">view all</Box>
      </Box>
      <Paper sx={{ height: 'fit-content', padding: '1rem', borderRadius: '8px' }}>
        <Grid container spacing={2}>
          {products?.result.map((product) => {
            return (
              <Grid
                item
                lg={2}
                md={3}
                sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
                key={product._id}
              >
                <Card component="div" sx={{ border: 'none', boxShadow: 'none' }}>
                  <Link to={`/products/${product._id}`}>
                    <CardMedia
                      component="img"
                      sx={{
                        width: '20vw',
                        height: '10rem',
                        borderRadius: '8px',
                        transition: 'all 100ms ease-in-out',
                        opacity: '1',
                        '&:hover': {
                          filter: 'brightness(50%)',
                        },
                      }}
                      image={product.thumbnail}
                      alt={product.title}
                    />

                    <Typography noWrap>{product.title}</Typography>
                    <Typography>
                      <DisplayCurrency number={product.price - (product.price / 100) * product.discountPercentage} />
                    </Typography>
                  </Link>
                </Card>
              </Grid>
            )
          })}
        </Grid>
      </Paper>
    </PageLayout>
  )
}

export default NewArrivals
