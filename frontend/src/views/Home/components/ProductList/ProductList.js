import {
  Box,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  IconButton,
  MenuItem,
  Pagination,
  Rating,
  TextField,
  Tooltip,
  Typography,
} from '@mui/material'
import { PageLayout } from 'layouts/Main/components'
import useSWR from 'swr'
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart'
import { DisplayCurrency } from 'components'
import { useState } from 'react'
import { useCart } from 'core'
import { Link } from 'react-router-dom'

const ProductList = () => {
  const [page, setPage] = useState(1)
  const [productsPerPage, setProductsPerPage] = useState(10)
  const { addToCart } = useCart()

  const { data: products, error } = useSWR(`/products?page=${page}&productsPerPage=${productsPerPage}`)

  return (
    <PageLayout error={error} data={products}>
      <Grid container spacing={3}>
        {products?.result.map((product) => (
          <Grid item xs={12} sm={4} md={3} key={product._id}>
            <Card
              sx={{
                boxShadow: (theme) => theme.shadows[1],
                '&:hover': {
                  boxShadow: (theme) => theme.shadows[15],
                },
              }}
            >
              <Link to={`/products/${product._id}`}>
                <CardMedia component="img" height="200" image={product.thumbnail} alt={product.title} />
              </Link>
              <CardContent>
                <Tooltip title={product.title} placement="bottom-start">
                  <Typography gutterBottom variant="h6" component="div" noWrap>
                    {product.title}
                  </Typography>
                </Tooltip>
                <Typography variant="body2" color="text.secondary" noWrap sx={{ mb: 3 }}>
                  {product.brand}
                </Typography>
                <Rating name="read-only" value={product.rating} readOnly size="small" />
              </CardContent>
              <CardActions>
                <Box display="flex" justifyContent="space-between" alignItems="center" sx={{ width: '100%' }}>
                  <Typography variant="body2" component="div">
                    {product.discountPercentage > 0 ? (
                      <>
                        <Box display="flex" alignItems="center">
                          <Box>
                            <DisplayCurrency
                              number={product.price - (product.price / 100) * product.discountPercentage}
                            />
                          </Box>
                          <Box sx={{ textDecoration: 'line-through', ml: 1, color: 'error.main' }}>
                            <DisplayCurrency number={product.price} />
                          </Box>
                        </Box>
                      </>
                    ) : (
                      <DisplayCurrency number={product.price} />
                    )}
                  </Typography>
                  <IconButton color="primary" onClick={() => addToCart(product)}>
                    <AddShoppingCartIcon />
                  </IconButton>
                </Box>
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
      <Box display="flex" alignItems="center" justifyContent="space-between" mt={4}>
        <Pagination
          count={products?.pages}
          page={page}
          onChange={(event, value) => {
            setPage(value)
          }}
        />
        <Box display="flex" alignItems="center">
          <Typography variant="overline" sx={{ mr: 1 }}>
            Products per page:
          </Typography>
          <TextField
            size="small"
            select
            value={productsPerPage}
            onChange={(e) => {
              setPage(1)
              setProductsPerPage(e.target.value)
            }}
          >
            <MenuItem value={10}>10</MenuItem>
            <MenuItem value={20}>20</MenuItem>
            <MenuItem value={30}>30</MenuItem>
          </TextField>
        </Box>
      </Box>
    </PageLayout>
  )
}

export default ProductList
