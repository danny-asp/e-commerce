// import { PageLayout } from 'layouts/Main/components'
// import Paper from '@mui/material/Paper'
// import { Box } from '@mui/system'
// import { Button, Menu, MenuItem } from '@mui/material'
// import Divider from '@mui/material/Divider'
// import { React, useState } from 'react'
// import useSWR from 'swr'

// const SubMenu = () => {
//   const [anchorEl, setAnchorEl] = useState(null)
//   const open = Boolean(anchorEl)
//   const handleClick = (event) => {
//     setAnchorEl(event.currentTarget)
//   }
//   const handleClose = () => {
//     setAnchorEl(null)
//   }

//   const { data } = useSWR(`/products?page=1&productsPerPage=40`)
//   let updadedArray = []
//   const uniqueCategories = data?.result.filter((element) => {
//     if (!updadedArray.includes(element.category)) {
//       updadedArray.push(element.category)
//       return element
//     }
//   })

//   return (
//     <PageLayout data="2" withContainer={false}>
//       <Paper
//         sx={{
//           paddigTop: '0',
//           width: '100%',
//           height: '3.5rem',
//           display: { xs: 'none', lg: 'flex', xl: 'flex' },
//           alignItems: 'center',
//           justifyContent: 'center',
//         }}
//       >
//         <Box
//           sx={{
//             width: '100%',
//             maxWidth: '1152px',
//             display: 'flex',
//             alignItems: 'center',
//             justifyContent: 'space-between',
//           }}
//         >
//           <Box>
//             <div>
//               <Button
//                 id="basic-button"
//                 aria-controls={open ? 'basic-menu' : undefined}
//                 aria-haspopup="true"
//                 aria-expanded={open ? 'true' : undefined}
//                 onClick={handleClick}
//               >
//                 Dashboard
//               </Button>
//               <Menu
//                 id="basic-menu"
//                 anchorEl={anchorEl}
//                 open={open}
//                 onClose={handleClose}
//                 MenuListProps={{
//                   'aria-labelledby': 'basic-button',
//                 }}
//                 sx={{ boxShadow: 'none' }}
//               >
//                 {uniqueCategories?.map((product) => {
//                   return (
//                     <MenuItem onClick={handleClose} sx={{ boxShadow: 'none' }}>
//                       {product.category}
//                     </MenuItem>
//                   )
//                 })}
//               </Menu>
//             </div>
//           </Box>
//           <Box>
//             <Box component="span" ml={5}>
//               LINK
//             </Box>
//             <Box component="span" ml={5}>
//               LINK
//             </Box>
//             <Box component="span" ml={5}>
//               LINK
//             </Box>
//             <Box component="span" ml={5}>
//               LINK
//             </Box>
//           </Box>
//         </Box>
//       </Paper>
//       <Divider />
//     </PageLayout>
//   )
// }

// export default SubMenu
