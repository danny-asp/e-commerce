import { Box, IconButton, Typography } from '@mui/material'
import { PageLayout } from 'layouts/Main/components'

import { Card, Grid } from '@mui/material'
import DeliveryDiningIcon from '@mui/icons-material/DeliveryDining'
import SupportAgentIcon from '@mui/icons-material/SupportAgent'
import AssuredWorkloadIcon from '@mui/icons-material/AssuredWorkload'
import LocalPoliceOutlinedIcon from '@mui/icons-material/LocalPoliceOutlined'

const Support = () => {
  return (
    <PageLayout data="2">
      <Grid container spacing={3}>
        <Grid item xs={12} md={6} lg={3}>
          <Card
            sx={{
              display: 'flex-block',
              alignItems: 'center',
              justifyContent: 'center',
              width: '100%',
              padding: '2rem',
              borderRadius: '8px',
              boxShadow: (theme) => theme.shadows[4],
              '&:hover': {
                boxShadow: (theme) => theme.shadows[10],
              },
            }}
          >
            <Box
              component="div"
              sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: '100%' }}
            >
              <IconButton sx={{ alignItems: 'center' }}>
                <DeliveryDiningIcon />
              </IconButton>
            </Box>
            <Box component="div" sx={{ textAlign: 'center', marginTop: '10px', marginBottom: '20px' }}>
              <Typography variant="h5">Worldwide Delivery</Typography>
            </Box>
            <Box component="div" sx={{ textAlign: 'center', padding: '0 15%' }}>
              <Typography variant="body2">
                We offer competitive prices on our 100 million plus product any range.
              </Typography>
            </Box>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={3}>
          <Card
            sx={{
              display: 'flex-block',
              alignItems: 'center',
              justifyContent: 'center',
              width: '100%',
              padding: '2rem',
              lineHeight: '3rem',
              borderRadius: '8px',
              boxShadow: (theme) => theme.shadows[4],
              '&:hover': {
                boxShadow: (theme) => theme.shadows[10],
              },
            }}
          >
            <Box
              component="div"
              sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: '100%' }}
            >
              {' '}
              <IconButton sx={{ alignItems: 'center' }}>
                <LocalPoliceOutlinedIcon />
              </IconButton>
            </Box>
            <Box component="div" sx={{ textAlign: 'center', marginTop: '10px', marginBottom: '20px' }}>
              <Typography variant="h5">Worldwide Delivery</Typography>
            </Box>
            <Box component="div" sx={{ textAlign: 'center', padding: '0 15%' }}>
              <Typography variant="body2">
                We offer competitive prices on our 100 million plus product any range.
              </Typography>
            </Box>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={3}>
          <Card
            sx={{
              display: 'flex-block',
              alignItems: 'center',
              justifyContent: 'center',
              width: '100%',
              padding: '2rem',
              borderRadius: '8px',
              boxShadow: (theme) => theme.shadows[4],
              '&:hover': {
                boxShadow: (theme) => theme.shadows[10],
              },
            }}
          >
            <Box
              component="div"
              sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: '100%' }}
            >
              {' '}
              <IconButton sx={{ alignItems: 'center' }}>
                <AssuredWorkloadIcon />
              </IconButton>
            </Box>
            <Box component="div" sx={{ textAlign: 'center', marginTop: '10px', marginBottom: '20px' }}>
              <Typography variant="h5">Worldwide Delivery</Typography>
            </Box>
            <Box component="div" sx={{ textAlign: 'center', padding: '0 15%' }}>
              <Typography variant="body2">
                We offer competitive prices on our 100 million plus product any range.
              </Typography>
            </Box>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={3}>
          <Card
            sx={{
              display: 'flex-block',
              alignItems: 'center',
              justifyContent: 'center',
              width: '100%',
              padding: '2rem',
              borderRadius: '8px',
              boxShadow: (theme) => theme.shadows[4],
              '&:hover': {
                boxShadow: (theme) => theme.shadows[10],
              },
            }}
          >
            <Box
              component="div"
              sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: '100%' }}
            >
              {' '}
              <IconButton sx={{ alignItems: 'center' }}>
                <SupportAgentIcon />
              </IconButton>
            </Box>
            <Box component="div" sx={{ textAlign: 'center', marginTop: '10px', marginBottom: '20px' }}>
              <Typography variant="h5">Worldwide Delivery</Typography>
            </Box>
            <Box component="div" sx={{ textAlign: 'center', padding: '0 15%' }}>
              <Typography variant="body2">
                We offer competitive prices on our 100 million plus product any range.
              </Typography>
            </Box>
          </Card>
        </Grid>
      </Grid>
    </PageLayout>
  )
}

export default Support
