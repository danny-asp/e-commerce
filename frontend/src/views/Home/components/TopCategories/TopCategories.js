import { Box, CardMedia, Typography } from '@mui/material'
import { PageLayout } from 'layouts/Main/components'
import CategoryIcon from '@mui/icons-material/Category'
import { Card, Grid } from '@mui/material'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/pagination'
import 'swiper/css/navigation'
import { Pagination, Navigation, Scrollbar, A11y } from 'swiper'
import useSWR from 'swr'

const TopCategories = () => {
  const { data } = useSWR(`/products?page=1&productsPerPage=40`)
  let updadedArray = []
  const uniqueCategories = data?.result.filter((element) => {
    if (!updadedArray.includes(element.category)) {
      updadedArray.push(element.category)
      return element
    }
  })

  return (
    <PageLayout data="2">
      <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', marginBottom: '1rem' }}>
        <Box component="span" sx={{ display: 'flex', alignItems: 'center' }}>
          <CategoryIcon sx={{ color: '#D23F57' }} />
          <Typography variant="h5" sx={{ marginLeft: '0.5rem', fontWeight: '600' }}>
            Top Categories
          </Typography>
        </Box>
        <Box component="span">view all</Box>
      </Box>

      <Swiper
        loop={true}
        modules={[Navigation, Pagination, Scrollbar, A11y]}
        spaceBetween={20}
        slidesPerView={1}
        breakpoints={{
          500: { slidesPerView: 2 },
          900: { slidesPerView: 3 },
        }}
        navigation={{ nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' }}
        pagination={false}
        scrollbar={{ draggable: true }}
        sx={{ border: '1px solid black', padding: '0 2rem 0 2rem', height: '9.5rem' }}
      >
        <Grid container spacing={3}>
          {uniqueCategories?.map((product) => {
            return (
              <Grid item xs={12} md={4}>
                <SwiperSlide>
                  <Card
                    sx={{
                      height: '120px',
                      padding: '1.2rem',
                      borderRadiu: '8px',
                    }}
                  >
                    <Box
                      sx={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: '100%',
                        '&:hover': {
                          filter: 'brightness(50%)',
                        },
                      }}
                    >
                      <CardMedia
                        component="img"
                        height="100%"
                        image={product.images[0]}
                        alt={product.title}
                        sx={{ width: '33%' }}
                      />

                      <CardMedia
                        component="img"
                        height="100%"
                        image={product.images[2]}
                        alt={product.title}
                        sx={{ width: '33%' }}
                      />
                      <CardMedia
                        component="img"
                        height="100%"
                        image={product.images[1]}
                        alt={product.title}
                        sx={{ width: '33%' }}
                      />
                    </Box>
                    <Box
                      sx={{
                        position: 'absolute',
                        top: '1rem',
                        left: '1rem',
                        display: 'inline-flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: '#0F3460',
                        borderRadius: '16px',
                        height: '24px',
                        whiteSpace: 'nowrap',
                        fontSize: '10px',
                        padding: '0 8px',
                        fontWeight: '600',
                        color: 'white',
                      }}
                    >
                      {product.category}
                    </Box>
                    <Box
                      sx={{
                        position: 'absolute',
                        top: '1rem',
                        right: '1rem',
                        display: 'inline-flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: 'rgba(0, 0, 0, 0.08)',
                        borderRadius: '16px',
                        height: '24px',
                        whiteSpace: 'nowrap',
                        fontSize: '10px',
                        padding: '0 8px',
                        fontWeight: '600',
                        color: '#2B3445',
                      }}
                    >
                      3k orders this week
                    </Box>
                  </Card>
                </SwiperSlide>
              </Grid>
            )
          })}
        </Grid>

        <Box
          class="swiper-button-prev"
          sx={{
            width: '2rem',
            heigth: '12rem',
            border: '1px solid black',
            backgroundColor: 'lightblue',
            position: 'absolute',
            top: '2%',
            left: '-2%',
            zIndex: '12',
          }}
        ></Box>
        <Box component="div" class="swiper-button-next" width="0.2rem" height="2rem"></Box>
      </Swiper>
    </PageLayout>
  )
}

export default TopCategories
