import { Box, Card, CardMedia, Paper, Typography } from '@mui/material'
import { PageLayout } from 'layouts/Main/components'
import LocalPoliceIcon from '@mui/icons-material/LocalPolice'
import StarBorderPurple500Icon from '@mui/icons-material/StarBorderPurple500'
import { Grid } from '@mui/material'
import useSWR from 'swr'
import { DisplayCurrency } from 'components'
import { Link } from 'react-router-dom'

const TopRating = () => {
  const { data: products, error } = useSWR(`/products?page=2&productsPerPage=4`)
  const { data: futureBrands } = useSWR(`/products?page=1&productsPerPage=2`)

  return (
    <PageLayout data="2" error={error} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', marginBottom: '2rem' }}>
            <Box component="span" sx={{ display: 'flex', alignItems: 'center' }}>
              <LocalPoliceIcon sx={{ color: '#D23F57' }} />
              <Typography variant="h5" sx={{ marginLeft: '0.5rem', fontWeight: '600' }}>
                Top Ratings
              </Typography>
            </Box>
            <Box component="span">view all</Box>
          </Box>
          <Paper sx={{ height: 'fit-content', boxShadow: '0px 1px 3px rgba(3, 0, 71, 0.09)' }}>
            <Grid
              container
              spacing={2}
              sx={{ padding: '1rem', display: 'flex', alignItems: 'center', justifyContent: 'center' }}
            >
              {products?.result.map((product) => {
                return (
                  <Grid
                    item
                    sm={6}
                    md={4}
                    lg={3}
                    sx={{ display: 'block', alignItems: 'center', justifyContent: 'center' }}
                  >
                    <Link to={`/products/${product._id}`}>
                      <Box sx={{ borderRadius: '8px', height: '200' }}>
                        <CardMedia
                          component="img"
                          height="120"
                          sx={{
                            width: '100%',
                            whiteSpace: 'nowrap',
                            borderRadius: '8px',
                            transition: 'all 100ms ease-in-out',
                            opacity: '1',
                            '&:hover': {
                              filter: 'brightness(50%)',
                            },
                          }}
                          image={product.thumbnail}
                          alt={product.title}
                        />
                      </Box>
                      <Typography noWrap style={{ textDecoration: 'none' }}>
                        {product.title}
                      </Typography>
                      <Typography>
                        <DisplayCurrency number={product.price} />
                      </Typography>
                    </Link>
                  </Grid>
                )
              })}
            </Grid>
          </Paper>
        </Grid>
        <Grid item xs={12} md={6}>
          <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', marginBottom: '1rem' }}>
            <Box component="span" sx={{ display: 'flex', alignItems: 'center' }}>
              <StarBorderPurple500Icon sx={{ color: '#D23F57' }} />
              <Typography variant="h5" sx={{ marginLeft: '0.5rem', fontWeight: '600' }}>
                Featured Brands
              </Typography>
            </Box>
            <Box component="span">viev all</Box>
          </Box>
          <Paper
            variant="elevation"
            sx={{
              height: '12.8rem',
              boxShadow: '0px 1px 3px rgba(3, 0, 71, 0.09)',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              padding: '1.5rem',
            }}
          >
            <Grid container spacing={2}>
              {futureBrands?.result.map((product) => {
                return (
                  <Grid item sm={6} sx={{ display: 'inline-flex', alignItems: 'center', justifyContent: 'center' }}>
                    <Card sx={{ border: 'none', boxShadow: 'none' }}>
                      <CardMedia
                        component="img"
                        sx={{
                          width: '13rem',
                          whiteSpace: 'nowrap',
                          height: '10rem',
                          borderRadius: '8px',
                          transition: 'all 100ms ease-in-out',
                          opacity: '1',
                          '&:hover': {
                            filter: 'brightness(50%)',
                          },
                        }}
                        image={product.thumbnail}
                        alt={product.title}
                      />

                      <Typography>{product.brand}</Typography>
                    </Card>
                  </Grid>
                )
              })}
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </PageLayout>
  )
}

export default TopRating
