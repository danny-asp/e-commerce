import { PageLayout } from 'layouts/Main/components'
import useSWR from 'swr'
import { Box, Paper, Typography, Grid, Button, CardMedia } from '@mui/material'
import { useCart } from 'core'
import { DisplayCurrency } from 'components'
import { useNavigate } from 'react-router-dom'
import { DetailsSummary } from 'views/Cart/components'
import LocalMallOutlinedIcon from '@mui/icons-material/LocalMallOutlined'

const OrderDetails = () => {
  const orderDetails = JSON.parse(localStorage.orderDetails ?? null)
  const address = JSON.parse(localStorage.address)
  const navigate = useNavigate()
  const { data, error } = useSWR(`/checkouts/${orderDetails.id}`)
  const { cart } = useCart()

  const continueShopping = () => {
    localStorage.removeItem('address')
    localStorage.removeItem('cartSnapshot')
    localStorage.removeItem('orderDetails')
    localStorage.removeItem('cart')
    localStorage.removeItem('details')
    localStorage.removeItem('voucher')
    navigate('/')
  }

  const getHoursAndMinutes = (date) => {}

  return (
    <PageLayout data={data} error={error} withContainer={true}>
      <Box component="div" mb={2} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
        <Box component="div">
          <Box component="span" sx={{ display: 'flex', alignItems: 'center' }}>
            <LocalMallOutlinedIcon sx={{ color: '#D23F57' }} />
            <Typography variant="h4" sx={{ marginLeft: '0.5rem', fontWeight: '600' }}>
              Order Review
            </Typography>
          </Box>
        </Box>
        <Box component="div">
          <Button
            onClick={continueShopping}
            variant="contained"
            sx={{
              width: '100%',
              marginTop: '2rem',
              boxShadow: 'none',
              color: (theme) => theme.palette.red.dark,
              backgroundColor: (theme) => theme.palette.red.light,
              filter: 'brightness(90%)',
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: (theme) => theme.palette.red.dark,
                color: (theme) => theme.palette.grey[200],
                filter: 'brightness(100%)',
              },
            }}
          >
            Order again
          </Button>
        </Box>
      </Box>
      <Paper
        sx={{
          borderRadius: (theme) => theme.borderRadius,
          transition: 'box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
          boxShadow: (theme) => theme.boxShadow,
          marginBottom: '2rem',
          display: 'flex',
          justifyContent: 'center',
          width: '100%',
        }}
      >
        <Box>
          <Box component="div">
            <Typography variant="h3">Your Order Is Confirmed</Typography>
          </Box>
          {/* <Box component="div">bitton</Box> */}
        </Box>
      </Paper>
      <Paper
        sx={{
          borderRadius: (theme) => theme.borderRadius,
          transition: 'box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
          boxShadow: (theme) => theme.boxShadow,
          marginBottom: '2rem',
          display: 'flex',
          justifyContent: 'center',
          width: '100%',
        }}
      >
        <Box component="div" sx={{ width: '100%', heigth: 'fit-content' }}>
          <Box
            component="div"
            sx={{
              width: '100%',
              heigth: '2rem',
              backgroundColor: (theme) => theme.palette.grey[400],
              borderRadius: '8px 8px 0px 0px',
              display: 'flex',
            }}
          >
            <Typography variant="body2" sx={{ margin: '0.5rem 1rem 0.5rem 1rem' }}>
              OrderId:{data?._id}
            </Typography>
            {/* <Typography variant="body2" sx={{ margin: '0.5rem 1rem 0.5rem 1rem' }}>
              Placed on:{data?.createdAt}
            </Typography>
            <Typography variant="body2" sx={{ margin: '0.5rem 1rem 0.5rem 1rem' }}>
              Delivered on:{data?.updatedAt}
            </Typography> */}
          </Box>
          {data?.cart.map((element) => {
            const productInformation = cart.find((product) => product._id === element.product._id)
            const priceOfCurProduct =
              productInformation.discountPercentage > 0
                ? productInformation.price - (productInformation.price / 100) * productInformation.discountPercentage
                : productInformation.price

            return (
              <Box component="div" sx={{ margin: '2rem 1rem' }}>
                <Grid container spacing={1}>
                  <Grid item xs={4}>
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                      <Box sx={{ width: '4rem', height: '4rem', objectFit: 'cover' }}>
                        <CardMedia
                          component="img"
                          height="64"
                          image={productInformation?.thumbnail}
                          alt={productInformation?.title}
                          sx={{ borderRadius: (theme) => theme.borderRadius }}
                        />
                      </Box>
                      <Box sx={{ marginLeft: '1rem' }}>
                        <Typography variant="body">{productInformation?.title}</Typography>
                        <Typography variant="body2">
                          <DisplayCurrency number={priceOfCurProduct * element.quantity} />
                        </Typography>
                      </Box>
                    </Box>
                  </Grid>
                  <Grid item xs={4} sx={{ display: 'flex', alignItems: 'center' }}>
                    <Box sx={{ width: '100%' }}>
                      <Typography sx={{ textAlign: 'end' }}>{productInformation?.title}</Typography>
                    </Box>
                  </Grid>
                  <Grid item xs={4} sx={{ display: 'flex', alignItems: 'center' }}>
                    <Box sx={{ width: '100%' }}>
                      <Button sx={{ float: 'right' }}>Write a review</Button>
                    </Box>
                  </Grid>
                </Grid>
              </Box>
            )
          })}
        </Box>
      </Paper>

      <Grid container spacing={2}>
        <Grid item xs={12} lg={6}>
          <Paper
            sx={{
              borderRadius: (theme) => theme.borderRadius,
              transition: 'box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
              boxShadow: (theme) => theme.boxShadow,
              marginBottom: '2rem',
              padding: '1rem 2rem',
              //   display: 'flex',
              //   justifyContent: 'center',
              width: '100%',
            }}
          >
            <Box sx={{ display: 'block' }}>
              <Typography variant="h5" sx={{ marginBottom: '2rem' }}>
                Shipping Address:
              </Typography>
              <Typography variant="body" component="div">
                {address.address1}
              </Typography>
              {address.address2 && <Typography variant="body">{address.address2}</Typography>}
            </Box>
          </Paper>
        </Grid>
        <Grid item xs={12} lg={6}>
          <DetailsSummary />
        </Grid>
        <Grid item xs={12}>
          <Button
            onClick={continueShopping}
            variant="contained"
            sx={{
              width: '100%',
              marginTop: '2rem',
              boxShadow: 'none',
              color: (theme) => theme.palette.grey[100],
              backgroundColor: (theme) => theme.palette.red.dark,
              filter: 'brightness(90%)',
              '&:hover': {
                boxShadow: 'none',
                backgroundColor: (theme) => theme.palette.red.dark,
                filter: 'brightness(100%)',
              },
            }}
          >
            Continie shopping
          </Button>
        </Grid>
      </Grid>
    </PageLayout>
  )
}

export default OrderDetails
