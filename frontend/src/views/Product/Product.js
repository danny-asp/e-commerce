import { useParams } from 'react-router-dom'
import { Box } from '@mui/system'
import { useCart } from 'core'
import useSWR from 'swr'
import { PageLayout } from 'layouts/Main/components'

import { CardMedia, Grid, Rating, Typography, Button } from '@mui/material'
import { DisplayCurrency } from 'components'

const Product = () => {
  const { product } = useParams()
  const { addToCart } = useCart()
  const { data, error } = useSWR(`/products/${product}`)

  return (
    <PageLayout data={data} error={error}>
      <Grid container spacing={4}>
        <Grid item sm={12} md={6}>
          <Box
            component="div"
            sx={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              padding: '10%',
            }}
          >
            <CardMedia
              component="img"
              height="160"
              image={data?.thumbnail}
              alt={data?.title}
              sx={{ objectFit: 'cover', borderRadius: '8px', width: '12rem' }}
            />
          </Box>
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              padding: '5%',
            }}
          >
            {data?.images.map((image, index) => {
              return (
                <CardMedia
                  component="img"
                  height="64"
                  image={image}
                  alt={data?.title}
                  sx={{ objectFit: 'cover', borderRadius: '8px', width: '4rem', marginLeft: '1rem' }}
                />
              )
            })}
          </Box>
        </Grid>
        <Grid item sm={12} md={6}>
          <Box component="div" sx={{ width: '100%' }}>
            <Typography variant="h3" sx={{ marginBottom: '1rem' }}>
              {data?.title}
            </Typography>
            <Typography variant="h6" sx={{ marginBottom: '1rem' }}>
              Brand: {data?.brand}
            </Typography>
            <Typography
              variant="h6"
              sx={{ display: 'flex', alignItems: 'center', justifyContent: 'start', marginBottom: '1rem' }}
            >
              Rated:
              <Rating name="read-only" value={data?.rating} readOnly size="small" />
            </Typography>
            <Typography variant="h4" color={(theme) => theme.palette.red.dark}>
              <DisplayCurrency number={data?.price - (data?.price / 100) * data?.discountPercentage} />
            </Typography>
            <Typography variant="body2">{data?.stock > 0 ? 'Stock Available' : 'Stock Unavailable'}</Typography>
            <Button
              onClick={() => addToCart(data)}
              variant="contained"
              sx={{
                boxShadow: 'none',
                color: (theme) => theme.palette.grey,
                backgroundColor: (theme) => theme.palette.red.dark,
                '&:hover': {
                  boxShadow: 'none',
                  backgroundColor: (theme) => theme.palette.red.light,
                  color: (theme) => theme.palette.red.dark,
                },
              }}
            >
              Buy Product
            </Button>
          </Box>
        </Grid>
      </Grid>
      <Box component="div">{data?.title}</Box>
    </PageLayout>
  )
}

export default Product
